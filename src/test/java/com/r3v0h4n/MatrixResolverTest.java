package com.r3v0h4n;

import org.junit.*;
import org.junit.runners.Parameterized;;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class MatrixResolverTest {
    private final String[] inputMatrix;
    private final int expectedResult;

    public MatrixResolverTest(String[] matrix, int result) {
        this.inputMatrix = matrix;
        this.expectedResult = result;
    }

    @Parameterized.Parameters(name = "{index}: expected result - {1}")
    public static Iterable<Object[]> inputMatrices() {
        return Arrays.asList(new Object[][] {
            { new String[] {
                "0111",
                "0010"
                }, 3
            },
            { new String[] {
                "0111",
                "0111"
                }, 6
            },
            { new String[] {
                "100111",
                "111111",
                "011111"
                }, 10
            },
            { new String[] {
                "10100",
                "10111",
                "11111",
                "10010"
                }, 6
            },
            { new String[] {
                "1011",
                "0011",
                "0111",
                "1111"
                }, 8
            },
            { new String[] {
                "101",
                "111",
                "001"
                }, 3
            },
            { new String[] {
                "1111"
                }, 4
            },
            { new String[] {
                "000",
                "000"
                }, 0
            },
            { new String[] {
                "0000111111001",
                "0001111001111"
                }, 6
            },
            { new String[] {
                "00",
                "00",
                "00",
                "10",
                "11",
                "11",
                "11",
                "01",
                "01",
                "11",
                "10",
                "10",
                "11"
                }, 6
            },
            { new String[] {
                "11100",
                "11000",
                "11110"
                }, 6
            },
        });
    }
    @Before
    public void printInputMatrix() {
        System.out.println();
        for(String vector: inputMatrix) {
            System.out.println(vector);
        }
    }
    @Test
    public void testResolve() {
        assertEquals(this.expectedResult, new MatrixResolver(inputMatrix).resolve());
    }
}