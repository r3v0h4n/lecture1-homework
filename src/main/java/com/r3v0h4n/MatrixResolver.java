package com.r3v0h4n;

import java.util.Arrays;
import java.util.Stack;


public class MatrixResolver {
    private final int[][] pMatrix;

    private final int rowLength;
    private final int colLength;
    private int maxSquare;

    public MatrixResolver(String[] strArr) {
        int[][] matrix = Arrays.stream(strArr)
                .map(str -> str.chars()
                        .map(Character::getNumericValue)
                        .toArray())
                .toArray(int[][]::new);

        this.pMatrix = this.constructPMatrix(matrix);
        this.rowLength = matrix.length;
        this.colLength = matrix[0].length;
        this.maxSquare = 0;
    }

    public int resolve() {
        this.maxSquare = 0;
        for (int j = 0; j < this.colLength; j++) {
            this.largestSquareInHistogram(j);
        }
        return this.maxSquare;
    }
    // For each index (i, j), if matrix[i][j] equals 1, then in pMatrix[i][j],
    // we will store the number of 1s to the right of the cell(i, j) along row ‘i’ before we encounter first zero
    // or end of the array plus 1.
    private int[][] constructPMatrix(int[][] matrix)
    {
        int rowLength = matrix.length;
        int colLength = matrix[0].length;
        int[][] pMatrix = new int[rowLength][colLength];

        for (int i = 0; i < rowLength; i++){
            for (int j = colLength - 1; j >= 0; j--)
            {
                if (matrix[i][j] == 0)
                    continue;

                if (j != colLength - 1)
                    pMatrix[i][j] += pMatrix[i][j + 1];

                pMatrix[i][j] += matrix[i][j];
            }
        }
        return pMatrix;
    }
    // Find max square of histogram from pMatrix column
    private void largestSquareInHistogram(int j) {
        Stack<Integer> stack = new Stack<>();
        int i = 0;
        while (i < this.rowLength)
        {
            // If this bar is higher than the bar on top stack, push it to stack
            if (stack.empty() || this.pMatrix[stack.peek()][j] <= this.pMatrix[i][j]) {
                stack.push(i++);

                // If this bar is lower than top of stack, then calculate area of rectangle
                // with stack top as the smallest (or minimum height) bar. 'i' is
                // 'right index' for the top and element before top in stack is 'left index'
            }
            else {
                this.popStack(stack, i, j);
            }
        }

        while (!stack.empty())
        {
            this.popStack(stack, i, j);
        }

    }
    // Pop the remaining bars from stack and calculate area with every popped bar as the smallest bar
    private void popStack(Stack<Integer> stack, int i, int j) {
        int top = stack.peek();
        stack.pop();
        int areaWithTop = this.pMatrix[top][j] * (stack.empty() ? i : i - stack.peek() - 1);

        if (this.maxSquare < areaWithTop)
            this.maxSquare = areaWithTop;
    }
}
