package com.r3v0h4n;

import java.util.Arrays;
import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please enter row count: ");
        int rowCount = scanner.nextInt();

        String[] matrix = new String[rowCount];
        System.out.println("Please enter " + rowCount + " row(s)");
        for (int i = 0; i < rowCount; i++) {
            matrix[i] = scanner.next();
        }


        MatrixResolver resolver = new MatrixResolver(matrix);
        int maxSquare = resolver.resolve();

        System.out.println("The largest sub matrix of 1's: " + maxSquare);
    }

}
